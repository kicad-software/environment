.PHONY: default
default: container.display



# ---

LOCATION.REPOSITORY:=$(shell pwd)
LOCATION.PROJECT:=$(LOCATION.REPOSITORY)/..
LOCATION.STORAGE:=$(LOCATION.REPOSITORY)/storage



# ---

CONTAINER:=kicad-build-environment
CONTAINER.TAG:=kicad/kicad-build-environment
CONTAINER.MOUNT:=\
--mount type=bind,source="$(LOCATION.STORAGE)",destination=/DISK/kicad/ \
--mount type=bind,source="$(LOCATION.STORAGE)"/user/default,destination=/root \
--mount type=bind,source="$(LOCATION.REPOSITORY)"/control,destination=/DISK/kicad/control \
--mount type=bind,source="$(LOCATION.REPOSITORY)/.."/eclipse,destination=/DISK/kicad/eclipse,readonly \
--mount type=bind,source="$(LOCATION.REPOSITORY)/.."/source,destination=/DISK/kicad/source \



CONTAINER.INSTANCE:=$(CONTAINER)-$(shell date +%s)
CONTAINER.USER:=--user $(shell id -u):$(shell id -g)
CONTAINER.HOSTNAME:=--hostname kicad-debug
CONTAINER.OPTIONS:=--name $(CONTAINER.INSTANCE) $(CONTAINER.MOUNT)



.PHONY: container.build
container.build:
	docker build --pull -t $(CONTAINER) -f container/$(CONTAINER) container
	docker tag $(CONTAINER) $(CONTAINER.TAG)

.PHONY: container.display
container.display:
	xhost +local:docker
	docker run $(CONTAINER.OPTIONS) -e DISPLAY=${DISPLAY} -v /tmp/.X11-unix:/tmp/.X11-unix -it $(CONTAINER)
	docker container rm $(CONTAINER.INSTANCE)

.PHONY: container.shell
container.shell:
	docker run $(CONTAINER.OPTIONS) -it $(CONTAINER)
	docker container rm $(CONTAINER.INSTANCE)

.PHONY: container.debug
container.debug:
	docker run $(CONTAINER.OPTIONS) -e DISPLAY=${DISPLAY} -v /tmp/.X11-unix:/tmp/.X11-unix -it $(CONTAINER) "control/debug"
	docker container rm $(CONTAINER.INSTANCE)

.PHONY: container.delete
container.delete:
	docker container rm $(CONTAINER.INSTANCE)

.PHONY: container.prune
container.prune:
	docker ps -qf 'name=^$(CONTAINER)*' -a | xargs docker container rm -f



# ---

.PHONY: debug
debug:
	echo debug



# ---

.PHONY: storage
storage:
	mkdir -p storage/user/default
	cp container/.bashrc storage/user/default

	mkdir -p storage/project/workspace
	mkdir -p storage/project/kicad
	mkdir -p storage/build